<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


/**
 * @OA\Parameter(
 *      name="user",
 *      in="path",
 *      description="User",
 *      required=true,
 *      @OA\Schema(
 *         type="integer"
 *      ),
 *      style="simple"
 *  ),
 */
class UserController extends Controller
{
    /**
     * @var User
     */
    protected $userModel;

    /**
     * UserController constructor.
     * @param User $userModel
     */
    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     path="/api/users",
     *     description="Get list of user",
     *     summary="Get list of user",
     *     tags={"Users"},
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Permission Denied",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     * )
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection($this->userModel->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/api/users",
     *     description="Add user",
     *     summary="Add user",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *     description="Store user",
     *         @OA\MediaType(
     *            mediaType="application/json",
     *            @OA\Schema(
     *                 allOf={
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="Name",
     *                             property="name",
     *                             type="string",
     *                         ),
     *                         @OA\Property(
     *                             description="Email",
     *                             property="email",
     *                             type="string",
     *                             enum="example@gmail.com",
     *                         ),
     *                         @OA\Property(
     *                             description="Password",
     *                             property="password",
     *                             type="string",
     *                             enum="string123",
     *                         ),
     *                         @OA\Property(
     *                             description="Password confirmation",
     *                             property="password_confirmation",
     *                             type="string",
     *                             enum="string123",
     *                         ),
     *                     ),
     *                 }
     *            )
     *        )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content (when list is not saved to campaign)",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Permission Denied",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     * )
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreUserRequest $request)
    {
        $user = new $this->userModel;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));

        $user->save();

        return (new UserResource($user))
            ->additional(['message' => 'User created!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete(
     *     path="/api/users/{user}",
     *     description="Destroy user",
     *     summary="Destroy user",
     *     tags={"Users"},
     *     @OA\Parameter(ref="#/components/parameters/user"),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=403, description="Permission Denied"),
     *     @OA\Response(response=404, description="Not found"),
     * )
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()
            ->json(['message' => 'User deleted!'])
            ->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }
}
