<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;

class TwitterController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/tweets/{user}",
     *     description="Get tweets of user",
     *     summary="Get tweets of user",
     *     tags={"Tweets"},
     *     @OA\Parameter(
     *         name="user",
     *         in="path",
     *         description="User",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *         style="simple"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Permission Denied",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     * )
     *
     * @param User $user
     * @return mixed
     */
    public function __invoke(User $user)
    {
        return \Twitter::getUserTimeline([
            'screen_name' => 'twitterapi',
            'count' => 10,
            'user_id' => $user->id,
            'format' => 'json'
        ]);
    }
}
