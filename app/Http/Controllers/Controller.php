<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         version="0.5.0",
 *         title="Text-box list service API"
 *     )
 * )
 *     @OA\Server(
 *          url="{schema}://test_project",
 *          description="OpenApi parameters",
 *          @OA\ServerVariable(
 *              serverVariable="schema",
 *              enum={"https", "http"},
 *              default="http"
 *          )
 *     )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
