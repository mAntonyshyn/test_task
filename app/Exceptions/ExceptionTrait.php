<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

trait ExceptionTrait
{

    /**
     * @param $request
     * @param $exception
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|Response
     * @throws \Exception
     */
    public function apiException($request, $exception)
    {
        if ($this->isModelException($exception)) {
            return $this->modelResponse($exception);
        }

        if ($this->isHttpException($exception) || $this->isRouteException($exception)) {
            return $this->httpResponse($exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * @param $exception
     * @return bool
     */
    protected function isModelException($exception)
    {
        return $exception instanceof ModelNotFoundException;
    }

    /**
     * @param $exception
     * @return bool
     */
    protected function isHttpException($exception)
    {
        return $exception instanceof NotFoundHttpException;
    }

    /**
     * @param $exception
     * @return bool
     */
    protected function isRouteException($exception)
    {
        return $exception instanceof RouteNotFoundException;
    }

    /**
     * @param $exception
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function modelResponse($exception)
    {
        $modelClass = explode('\\', $exception->getModel());

        return response([
            'error' => 'Model '.end($modelClass).' not found',
            'status' => Response::HTTP_NOT_FOUND
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param $exception
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function httpResponse($exception)
    {
        return response([
            'error' => 'Incorrect route name',
            'status' => Response::HTTP_NOT_FOUND
        ], Response::HTTP_NOT_FOUND);
    }
}

